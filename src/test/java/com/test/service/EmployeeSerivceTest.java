package com.test.service;


import com.test.model.Employee;
import com.test.repository.EmployeeRepository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;


import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeSerivceTest {
    private String cityName = "Bhopal";

    @Mock
    EmployeeRepository employeeRepository;

    @InjectMocks
    EmployeeService employeeService;


    @Test
    public void testEmployeeAge() {
        List<Employee> employeeList = new ArrayList<>();

        employeeList.add(new Employee("1", "Yash", 22, "Bhopal"));
        employeeList.add(new Employee("2", "R", 23, "Bhopal"));
        employeeList.add(new Employee("3", "Aaditya", 24, "Bhopal"));

        Mockito.when(employeeRepository.getByCity(cityName)).thenReturn(employeeList);
        int totalage = employeeService.getAgeSumByCity(cityName);
        Assert.assertEquals(69, totalage);


    }

}
