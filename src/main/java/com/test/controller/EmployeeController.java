package com.test.controller;

import com.test.model.Employee;
import com.test.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

@RestController
@EnableDiscoveryClient
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;


    @Autowired
    private DiscoveryClient discoveryClient;


    @GetMapping("/employee")
    public List<Employee> getEmployee(){
        List<Employee> employeeList = employeeService.getEmployee();

        return employeeList;
    }

    @GetMapping("/byId/{id}")
    public Employee getEmployeeById(@PathVariable String id){
        Employee employeeById = employeeService.getEmployeeById(id);
        return employeeById;
    }
//serialization convert into json
    @PostMapping("/addemp")
    public String saveEmployee(@RequestBody Employee employee){
        return employeeService.saveEmployee(employee);
    }

    @DeleteMapping("/deleteemp")
    public String deleteEmployee(@RequestBody Employee employee){
        return employeeService.deleteEmployee(employee);
    }

    @PostMapping("/addMultiple")
    public List<String> addMultiple(@RequestBody List<Employee> employees){
        return employeeService.addMultiple(employees);
    }

    @PutMapping("/updateEmployee/{id}")
    public String updateEmployee(@RequestBody Employee employee, @PathVariable String id){
        return employeeService.updateEmployee(employee, id);

    }


    @GetMapping("/api/getUserSalary/{id}")
    public Long getUserSalary(@PathVariable String id){
        Employee employeeAgeByID = employeeService.getEmployeeById(id);
        RestTemplate restTemplate = new RestTemplate();
        //long salary = restTemplate.getForObject("http://localhost:8081/get/" + employeeAgeByID.getAge(), Long.class);

        URI uri = discoveryClient.getInstances("SalaryApplication").stream().map(si -> si.getUri()).findFirst().map(s->s.resolve("/get/"+employeeAgeByID.getAge())).get();
        long salary = restTemplate.getForObject(uri, Long.class);

        return salary;





    }


    @GetMapping("/getByCity/{city}")
    public List<Employee> getByCity(@PathVariable String city){
        return employeeService.getByCity(city);
    }

    @GetMapping("/getAgeSum/{city}")
    public int getAgeSumByCity(@PathVariable String city){
        return employeeService.getAgeSumByCity(city);
    }



















}
