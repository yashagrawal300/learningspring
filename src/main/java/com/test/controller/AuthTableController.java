package com.test.controller;

import com.test.model.Authtable;
import com.test.service.AuthTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AuthTableController {
    @Autowired
    AuthTableService authTableService;

    @GetMapping("/getUsers")
    public List<Authtable> getUsers(){
        List<Authtable> authtables = authTableService.getUsers();
        return authtables;
    }


}
