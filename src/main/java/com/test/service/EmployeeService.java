package com.test.service;

import com.test.model.Employee;
import com.test.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

@Service

public class EmployeeService {
    @Autowired
    EmployeeRepository employeeRepository;


    public List<Employee> getEmployee(){
        List<Employee> employeeList = employeeRepository.findAll();

        return employeeList;
    }

    public Employee getEmployeeById(String id){

        Employee employeeById = employeeRepository.getEmployeeById(id);
        return employeeById;
    }

    public String saveEmployee(Employee employee){
        if(employeeRepository.existsById(employee.getId())){
            return "Id already exists";

        }
        employeeRepository.save(employee);
        return "Employee data saved";

    }

    public String deleteEmployee(Employee employee){
        employeeRepository.deleteById(employee.getId());
        return "Employee data deleted";
    }

    public List<String> addMultiple(List<Employee> employeeList){
        List<String> employee= new ArrayList<>();
        for(Employee E: employeeList){
            employee.add(saveEmployee(E));
        }
        return employee;

    }

    public String updateEmployee(Employee employee, String id){
        Employee employeeToUpdate = employeeRepository.getById(id);
        employeeToUpdate.setAge(employee.getAge());
        employeeToUpdate.setName(employee.getName());
        employeeToUpdate.setCity(employee.getCity());
        employeeRepository.save(employeeToUpdate);

        return "Updated";

    }

    public List<Employee> getByCity(String city){
        return employeeRepository.getByCity(city);
    }


    public int getAgeSumByCity(String city){
        List<Employee> employeeList = employeeRepository.getByCity(city);
        int a = 0;
        for(Employee e: employeeList){
            a+= e.getAge();
        }

        return a;

    }








}
