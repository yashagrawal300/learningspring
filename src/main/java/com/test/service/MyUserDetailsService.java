package com.test.service;

import com.test.model.Authtable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailsService implements UserDetailsService {
    @Autowired
    AuthTableService authTableService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Authtable user = authTableService.findByUsername(username);
        if(user == null){
            throw new UsernameNotFoundException("user not found");
        }
        return new UserPrincipal(user);
    }
}
