package com.test.service;

import com.test.model.Authtable;
import com.test.repository.AuthTableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthTableService {
    @Autowired
    AuthTableRepository authTableRepository;

    public List<Authtable> getUsers(){
        List<Authtable> authtables = authTableRepository.findAll();
        return authtables;
    }

    public Authtable findByUsername(String uName){
        return authTableRepository.findByUsername(uName);


    }


}
