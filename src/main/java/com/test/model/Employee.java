package com.test.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Employee {
    @Id
    private String id;
    private String Name;
    private int age;


    private String city;


    public Employee(){

    }

    public String getCity() {
        return city;
    }

    public Employee(String id, String name, int age, String city) {
        this.id = id;
        Name = name;
        this.age = age;
        this.city = city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
