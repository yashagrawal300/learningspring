package com.test.repository;

import com.test.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, String> {

    //* in sql will not work rather try writting with class attributes
    // Just in case if this does not work here but the query works just set ,native = True
    @Query("SELECT e FROM Employee e where e.city= :city")
    List<Employee> getByCity(String city);


    Employee getEmployeeById(String id);
}
