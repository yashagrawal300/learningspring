package com.test.repository;

import com.test.model.Authtable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthTableRepository extends JpaRepository<Authtable, String> {
Authtable findByUsername(String uName);

}
